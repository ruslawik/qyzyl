<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToDocumentsTable extends Migration
{
    public function up()
    {
        Schema::table('documents', function (Blueprint $table) {
            $table->unsignedInteger('city_id')->nullable();
            $table->foreign('city_id', 'city_fk_2338661')->references('id')->on('cities');
            $table->unsignedInteger('district_id')->nullable();
            $table->foreign('district_id', 'district_fk_2338662')->references('id')->on('districts');
            $table->unsignedInteger('celevoe_naznachenie_id')->nullable();
            $table->foreign('celevoe_naznachenie_id', 'celevoe_naznachenie_fk_2338667')->references('id')->on('goals');
            $table->unsignedInteger('fond_category_id')->nullable();
            $table->foreign('fond_category_id', 'fond_category_fk_2338668')->references('id')->on('fond_categories');
            $table->unsignedInteger('object_type_id')->nullable();
            $table->foreign('object_type_id', 'object_type_fk_2338669')->references('id')->on('object_types');
            $table->unsignedInteger('object_group_id')->nullable();
            $table->foreign('object_group_id', 'object_group_fk_2338670')->references('id')->on('object_groups');
        });
    }
}
