<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            $table->string('barcode')->nullable();
            $table->string('cadastrnum')->nullable();
            $table->string('address')->nullable();
            $table->string('litera')->nullable();
            $table->string('object_name')->nullable();
            $table->string('barcodeint')->nullable();
            $table->string('doc_number_old')->nullable();
            $table->integer('sheets_in_doc')->nullable();
            $table->string('is_secret');
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
