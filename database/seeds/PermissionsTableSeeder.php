<?php

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'spravochniki_access',
            ],
            [
                'id'    => 18,
                'title' => 'city_create',
            ],
            [
                'id'    => 19,
                'title' => 'city_edit',
            ],
            [
                'id'    => 20,
                'title' => 'city_show',
            ],
            [
                'id'    => 21,
                'title' => 'city_delete',
            ],
            [
                'id'    => 22,
                'title' => 'city_access',
            ],
            [
                'id'    => 23,
                'title' => 'district_create',
            ],
            [
                'id'    => 24,
                'title' => 'district_edit',
            ],
            [
                'id'    => 25,
                'title' => 'district_show',
            ],
            [
                'id'    => 26,
                'title' => 'district_delete',
            ],
            [
                'id'    => 27,
                'title' => 'district_access',
            ],
            [
                'id'    => 28,
                'title' => 'goal_create',
            ],
            [
                'id'    => 29,
                'title' => 'goal_edit',
            ],
            [
                'id'    => 30,
                'title' => 'goal_show',
            ],
            [
                'id'    => 31,
                'title' => 'goal_delete',
            ],
            [
                'id'    => 32,
                'title' => 'goal_access',
            ],
            [
                'id'    => 33,
                'title' => 'fond_category_create',
            ],
            [
                'id'    => 34,
                'title' => 'fond_category_edit',
            ],
            [
                'id'    => 35,
                'title' => 'fond_category_show',
            ],
            [
                'id'    => 36,
                'title' => 'fond_category_delete',
            ],
            [
                'id'    => 37,
                'title' => 'fond_category_access',
            ],
            [
                'id'    => 38,
                'title' => 'object_group_create',
            ],
            [
                'id'    => 39,
                'title' => 'object_group_edit',
            ],
            [
                'id'    => 40,
                'title' => 'object_group_show',
            ],
            [
                'id'    => 41,
                'title' => 'object_group_delete',
            ],
            [
                'id'    => 42,
                'title' => 'object_group_access',
            ],
            [
                'id'    => 43,
                'title' => 'object_type_create',
            ],
            [
                'id'    => 44,
                'title' => 'object_type_edit',
            ],
            [
                'id'    => 45,
                'title' => 'object_type_show',
            ],
            [
                'id'    => 46,
                'title' => 'object_type_delete',
            ],
            [
                'id'    => 47,
                'title' => 'object_type_access',
            ],
            [
                'id'    => 48,
                'title' => 'document_create',
            ],
            [
                'id'    => 49,
                'title' => 'document_edit',
            ],
            [
                'id'    => 50,
                'title' => 'document_show',
            ],
            [
                'id'    => 51,
                'title' => 'document_delete',
            ],
            [
                'id'    => 52,
                'title' => 'document_access',
            ],
            [
                'id'    => 53,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
