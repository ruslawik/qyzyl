<?php

Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Cities
    Route::apiResource('cities', 'CitiesApiController');

    // Districts
    Route::apiResource('districts', 'DistrictsApiController');

    // Goals
    Route::apiResource('goals', 'GoalApiController');

    // Fond Categories
    Route::apiResource('fond-categories', 'FondCategoryApiController');

    // Object Groups
    Route::apiResource('object-groups', 'ObjectGroupApiController');

    // Object Types
    Route::apiResource('object-types', 'ObjectTypeApiController');

    // Documents
    Route::post('documents/media', 'DocumentsApiController@storeMedia')->name('documents.storeMedia');
    Route::apiResource('documents', 'DocumentsApiController');
});
