<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);
// Admin

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/monitor', 'HomeController@monitor');

    Route::get('/download/{doc_id}', 'DocumentsController@downloadZip');
    Route::post('/add_file', 'DocumentsController@addFile');
	Route::post('/add_file_jur', 'DocumentsController@add_jur_File');
	Route::post('/add_file_jur2', 'DocumentsController@add_jur_File2');
    Route::get('/delete_file/{file_id}/{doc_number}', 'DocumentsController@deleteFile');
	Route::get('/delete_jur_file/{file_id}/{doc_number}', 'DocumentsController@deleteJurFile');
	Route::get('/delete_jur_file2/{file_id}/{doc_number}', 'DocumentsController@deleteJurFile2');

    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Cities
    Route::delete('cities/destroy', 'CitiesController@massDestroy')->name('cities.massDestroy');
    Route::resource('cities', 'CitiesController');

    // Districts
    Route::delete('districts/destroy', 'DistrictsController@massDestroy')->name('districts.massDestroy');
    Route::resource('districts', 'DistrictsController');

    // Goals
    Route::delete('goals/destroy', 'GoalController@massDestroy')->name('goals.massDestroy');
    Route::resource('goals', 'GoalController');

    // Fond Categories
    Route::delete('fond-categories/destroy', 'FondCategoryController@massDestroy')->name('fond-categories.massDestroy');
    Route::resource('fond-categories', 'FondCategoryController');

    // Object Groups
    Route::delete('object-groups/destroy', 'ObjectGroupController@massDestroy')->name('object-groups.massDestroy');
    Route::resource('object-groups', 'ObjectGroupController');

    // Object Types
    Route::delete('object-types/destroy', 'ObjectTypeController@massDestroy')->name('object-types.massDestroy');
    Route::resource('object-types', 'ObjectTypeController');

    // Documents
    Route::delete('documents/destroy', 'DocumentsController@massDestroy')->name('documents.massDestroy');
    Route::post('documents/media', 'DocumentsController@storeMedia')->name('documents.storeMedia');
    Route::post('documents/ckmedia', 'DocumentsController@storeCKEditorImages')->name('documents.storeCKEditorImages');
	Route::any('/documents/ajax-docs', 'DocumentsController@ajax_docs');
    Route::resource('documents', 'DocumentsController');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
// Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
