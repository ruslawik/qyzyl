<aside class="main-sidebar sidebar-dark-primary elevation-4" style="min-height: 917px;">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">{{ trans('panel.site_title') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user (optional) -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item" style="margin-bottom:10px;">
                    <a class="nav-link" href="{{ route("admin.home") }}">
                        <i class="fas fa-bell nav-icon">
                        </i>
                        <p>
                            {{ trans('global.dashboard') }}
                        </p>
                    </a>
                </li>
                @can('user_management_access')
                    <li class="nav-item has-treeview {{ request()->is("admin/permissions*") ? "menu-open" : "" }} {{ request()->is("admin/roles*") ? "menu-open" : "" }} {{ request()->is("admin/users*") ? "menu-open" : "" }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw nav-icon fas fa-users">

                            </i>
                            <p>
                                {{ trans('cruds.userManagement.title') }}
                                <i class="right fa fa-fw fa-angle-left nav-icon"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('permission_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.permissions.index") }}" class="nav-link {{ request()->is("admin/permissions") || request()->is("admin/permissions/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-unlock-alt">

                                        </i>
                                        <p>
                                            {{ trans('cruds.permission.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('role_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.roles.index") }}" class="nav-link {{ request()->is("admin/roles") || request()->is("admin/roles/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-briefcase">

                                        </i>
                                        <p>
                                            {{ trans('cruds.role.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('user_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.users.index") }}" class="nav-link {{ request()->is("admin/users") || request()->is("admin/users/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-user">

                                        </i>
                                        <p>
                                            {{ trans('cruds.user.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('spravochniki_access')
                    <li class="nav-item has-treeview {{ request()->is("admin/cities*") ? "menu-open" : "" }} {{ request()->is("admin/districts*") ? "menu-open" : "" }} {{ request()->is("admin/goals*") ? "menu-open" : "" }} {{ request()->is("admin/fond-categories*") ? "menu-open" : "" }} {{ request()->is("admin/object-groups*") ? "menu-open" : "" }} {{ request()->is("admin/object-types*") ? "menu-open" : "" }}">
                        <a class="nav-link nav-dropdown-toggle" href="#">
                            <i class="fa-fw nav-icon fas fa-list">

                            </i>
                            <p>
                                {{ trans('cruds.spravochniki.title') }}
                                <i class="right fa fa-fw fa-angle-left nav-icon"></i>
                            </p>
                        </a>
                        <ul class="nav nav-treeview">
                            @can('city_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.cities.index") }}" class="nav-link {{ request()->is("admin/cities") || request()->is("admin/cities/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-map-marked-alt">

                                        </i>
                                        <p>
                                            {{ trans('cruds.city.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('district_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.districts.index") }}" class="nav-link {{ request()->is("admin/districts") || request()->is("admin/districts/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-map-marker-alt">

                                        </i>
                                        <p>
                                            {{ trans('cruds.district.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('goal_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.goals.index") }}" class="nav-link {{ request()->is("admin/goals") || request()->is("admin/goals/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-crosshairs">

                                        </i>
                                        <p>
                                            {{ trans('cruds.goal.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('fond_category_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.fond-categories.index") }}" class="nav-link {{ request()->is("admin/fond-categories") || request()->is("admin/fond-categories/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-archway">

                                        </i>
                                        <p>
                                            {{ trans('cruds.fondCategory.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('object_group_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.object-groups.index") }}" class="nav-link {{ request()->is("admin/object-groups") || request()->is("admin/object-groups/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-ellipsis-h">

                                        </i>
                                        <p>
                                            {{ trans('cruds.objectGroup.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                            @can('object_type_access')
                                <li class="nav-item">
                                    <a href="{{ route("admin.object-types.index") }}" class="nav-link {{ request()->is("admin/object-types") || request()->is("admin/object-types/*") ? "active" : "" }}">
                                        <i class="fa-fw nav-icon fas fa-home">

                                        </i>
                                        <p>
                                            {{ trans('cruds.objectType.title') }}
                                        </p>
                                    </a>
                                </li>
                            @endcan
                        </ul>
                    </li>
                @endcan
                @can('document_access')
                    <li class="nav-item">
                        <a href="{{ route("admin.documents.index") }}" class="nav-link {{ request()->is("admin/documents") || request()->is("admin/documents/*") ? "active" : "" }}">
                            <i class="fa-fw nav-icon fas fa-file-alt">

                            </i>
                            <p>
                                {{ trans('cruds.document.title') }}
                            </p>
                        </a>
                    </li>
                @endcan
                @can('document_create')
                <li class="nav-item" style="margin-top:10px;">
                    <a style="margin-left:5px;color:white !important;" class="btn btn-success" href="{{ route('admin.documents.create') }}">
                         <i class="fa-fw nav-icon fas fa-plus">
                            </i>
                    {{ trans('global.add') }} {{ trans('cruds.document.title_singular') }}
                    </a>

                </li>
                @endcan
                @can('monitor')
                <li class="nav-item" style="margin-top:10px;">
                    <a class="nav-link {{ request()->is("admin/monitor") || request()->is("admin/monitor/*") ? "active" : "" }}" href="/admin/monitor">
                        <i class="fas fa-fw fa-tachometer-alt nav-icon">
                        </i>
                        <p>
                            Отчеты
                        </p>
                    </a>
                </li>
                @endcan
                <li class="nav-item" style="margin-top:10px;">
                    <a target="_blank" class="nav-link" href="https://youtu.be/Y1mgZebXEhc">
                        <i class="fas fa-question nav-icon">
                        </i>
                        <p>
                            Помощь
                        </p>
                    </a>
                </li>

                @if(file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php')))
                    @can('profile_password_edit')
                        <li class="nav-item" style="margin-top:10px;">
                            <a class="nav-link {{ request()->is('profile/password') || request()->is('profile/password/*') ? 'active' : '' }}" href="{{ route('profile.password.edit') }}">
                                <i class="fa-fw fas fa-key nav-icon">
                                </i>
                                <p>
                                    {{ trans('global.change_password') }}
                                </p>
                            </a>
                        </li>
                    @endcan
                @endif
                <li class="nav-item">
                    <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                        <p>
                            <i class="fas fa-fw fa-sign-out-alt nav-icon">

                            </i>
                            <p>{{ trans('global.logout') }}</p>
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>