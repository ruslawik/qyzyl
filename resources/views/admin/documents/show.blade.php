@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.document.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.documents.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.document.fields.id') }}
                        </th>
                        <td>
                            {{ $document->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Штрих код
                        </th>
                        <td>
                            <?php echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($document->barcode, 'C39',1.5,70,array(1,1,1), true) . '" alt="barcode"   />'; ?>
                            <br>
                            <a href="data:image/png;base64,<?php echo DNS1D::getBarcodePNG($document->barcode, 'C39',1.5,70,array(1,1,1), true); ?>" download="{{$document->barcode}}">Скачать</a>
                        </td>
                    </tr>
					<tr>
                        <th>
                            Инвентарный номер
                        </th>
                        <td>
                            {{ $document->invent_num }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.document.fields.city') }}
                        </th>
                        <td>
                            {{ $document->city->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.document.fields.district') }}
                        </th>
                        <td>
                            {{ $document->district->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.document.fields.cadastrnum') }}
                        </th>
                        <td>
                            {{ $document->cadastrnum }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.document.fields.address') }}
                        </th>
                        <td>
                            {{ $document->address }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.document.fields.litera') }}
                        </th>
                        <td>
                            {{ $document->litera }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.document.fields.object_name') }}
                        </th>
                        <td>
                            {{ $document->object_name }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.document.fields.celevoe_naznachenie') }}
                        </th>
                        <td>
                            {{ $document->celevoe_naznachenie->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.document.fields.fond_category') }}
                        </th>
                        <td>
                            {{ $document->fond_category->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.document.fields.object_type') }}
                        </th>
                        <td>
                            {{ $document->object_type->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.document.fields.object_group') }}
                        </th>
                        <td>
                            {{ $document->object_group->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.document.fields.is_secret') }}
                        </th>
                        <td>
                            {{ App\Models\Document::IS_SECRET_SELECT[$document->is_secret] ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                           Инвентарные дела
                            <hr>
                            <a href="/admin/download/{{$document->id}}">Скачать все файлы архивом</a><hr>
                            @foreach($document->files($document->doc_number_old) as $file)
                                <?php
                                    $two_parts = explode(".", $file->filename);
                                    $extension = $two_parts[1];
                                ?>
                                @if($extension == "pdf")
                                    <a href="#/" onClick="show_pdf('/storage/documents/{{$document->doc_number_old}}/{{$file->filename}}');">{{$file->filename}}</a><br>
                                @else
                                    <a href="#/" onClick="show_file('/storage/documents/{{$document->doc_number_old}}/{{$file->filename}}');">{{$file->filename}}</a><br>
                                @endif

                            @endforeach
                        </th>
                        <td>
                            <script src="/js/tiff.min.js"></script>
                            <style>
                                .doc_for canvas{
                                    width:600px !important;
                                }
                            </style>
                            <div id="doc" class="doc_for"></div>
                        </td>
                    </tr>
					<tr>
                        <th>
                           Регистрационные дела
                            <hr>
                            <a href="/admin/download/{{$document->id}}">Скачать все файлы архивом</a><hr>
                            @foreach($document->jur_files($document->doc_number_old) as $file)
                                <?php
                                    $two_parts = explode(".", $file->filename);
                                    $extension = $two_parts[1];
                                ?>
                                @if($extension == "pdf")
                                    <a href="#/" onClick="show_pdf2('/storage/documents/{{$document->doc_number_old}}/{{$file->filename}}');">{{$file->filename}}</a><br>
                                @else
                                    <a href="#/" onClick="show_file2('/storage/documents/{{$document->doc_number_old}}/{{$file->filename}}');">{{$file->filename}}</a><br>
                                @endif
                            @endforeach
                        </th>
                        <td>
                            <script src="/js/tiff.min.js"></script>
                            <style>
                                .doc_for canvas{
                                    width:600px !important;
                                }
                            </style>
                            <div id="doc2" class="doc_for"></div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <h4>История документа</h4>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Автор</th>
                        <th>Действие</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($journal_actions as $action)
                    <tr>
                        <td>{{$action->user($action->author_id)}}</td>
                        <td>{!!$action->action!!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.documents.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
@section('scripts')
    <script>
        function show_file(url){
                                    $("#doc").empty();
                                    $("#doc").html("<a href='"+url+"'>Скачать полный файл</a><br><br>");
                                    var xhr = new XMLHttpRequest();
                                    xhr.responseType = 'arraybuffer';
                                    xhr.open('GET', url);
                                    xhr.onload = function (e) {
                                        var tiff = new Tiff({buffer: xhr.response});
                                        var canvas = tiff.toCanvas();
                                        document.getElementById("doc").append(canvas);
                                        $([document.documentElement, document.body]).animate({
                                            scrollTop: $("#doc").offset().top
                                        }, 1000);
                                    };
                                    xhr.send();
        }
        function show_pdf(url){
                $("#doc").empty();
                $("#doc").html("<a target='_blank' href='"+url+"'>Скачать полный файл</a><br><br>")
                $("#doc").append('<div><object data="'+url+'" type="application/pdf" width="600" height=600px></object></div>');
                $([document.documentElement, document.body]).animate({scrollTop: $("#doc").offset().top}, 1000);
        }
        function show_file2(url){
                                    $("#doc2").empty();
                                    $("#doc2").html("<a href='"+url+"'>Скачать полный файл</a><br><br>");
                                    var xhr = new XMLHttpRequest();
                                    xhr.responseType = 'arraybuffer';
                                    xhr.open('GET', url);
                                    xhr.onload = function (e) {
                                        var tiff = new Tiff({buffer: xhr.response});
                                        var canvas = tiff.toCanvas();
                                        document.getElementById("doc2").append(canvas);
                                        $([document.documentElement, document.body]).animate({
                                            scrollTop: $("#doc2").offset().top
                                        }, 1000);
                                    };
                                    xhr.send();
        }
        function show_pdf2(url){
                $("#doc2").empty();
                $("#doc2").html("<a target='_blank' href='"+url+"'>Скачать полный файл</a><br><br>")
                $("#doc2").append('<div><object data="'+url+'" type="application/pdf" width="600" height=600px></object></div>');
                $([document.documentElement, document.body]).animate({scrollTop: $("#doc2").offset().top}, 1000);
        }
    </script>
@endsection