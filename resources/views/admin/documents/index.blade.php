@extends('layouts.admin')
@section('content')

<div class="card card-warning">
              <div class="card-header">
                <h3 class="card-title">Поиск документов</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form role="form" method="GET" action="{{ route('admin.documents.index') }}">
                  <div class="row">
                    <div class="col-sm-2">
                      <!-- text input -->
                      <div class="form-group">
                        <label>Город</label>
                        <select class="form-control" name="city_id" id="city_id">
                            <option value="-1">{{ trans('global.all') }}</option>
                            @foreach($cities as $key => $item)
                                @if(isset($_GET['city_id']) && $item->id == $_GET['city_id'])
                                    <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <!-- text input -->
                      <div class="form-group">
                        <label>Район</label>
                        <select class="form-control" name="district_id" id="district_id">
                            <option value="-1">{{ trans('global.all') }}</option>
                            @foreach($districts as $key => $item)
                                @if(isset($_GET['district_id']) && $item->id == $_GET['district_id'])
                                    <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Кадастровый номер</label>
                        <input type="text" class="form-control" name="cad_num" id="cad_num" @if(isset($_GET['cad_num'])) value="{{$_GET['cad_num']}}" @endif>
                      </div>
                    </div>
                      <div class="col-sm-2">
                          <div class="form-group">
                              <label>Инв. № рег. дела</label>
                              <input type="text" class="form-control" name="inv_nom_reg_del" id="inv_nom_reg_del" @if(isset($_GET['inv_nom_reg_del'])) value="{{$_GET['inv_nom_reg_del']}}" @endif>
                          </div>
                      </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Адрес</label>
                        <input type="text" class="form-control" name="address" id="address" @if(isset($_GET['address'])) value="{{$_GET['address']}}" @endif>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Литера</label>
                        <input type="text" class="form-control" name="litera" id="litera" @if(isset($_GET['litera'])) value="{{$_GET['litera']}}" @endif>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Штрих-код</label>
                        <input type="text" class="form-control" name="wtrix" id="wtrix" @if(isset($_GET['wtrix'])) value="{{$_GET['wtrix']}}" @endif>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Инвент. номер</label>
                        <input type="text" class="form-control" name="invent_num" id="invent_num" @if(isset($_GET['invent_num'])) value="{{$_GET['invent_num']}}" @endif>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>ИИН</label>
                        <input type="text" class="form-control" name="iin" id="iin" @if(isset($_GET['iin'])) value="{{$_GET['iin']}}" @endif>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Целевое назначение</label>
                        <select class="form-control" name="goal_id" id="goal_id">
                            <option value="-1">{{ trans('global.all') }}</option>
                            @foreach($goals as $key => $item)
                                @if(isset($_GET['goal_id']) && $item->id == $_GET['goal_id'])
                                    <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Тип объекта</label>
                        <select class="form-control" name="object_type_id" id="object_type_id">
                            <option value="-1">{{ trans('global.all') }}</option>
                            @foreach($object_types as $key => $item)
                                @if(isset($_GET['object_type_id']) && $item->id == $_GET['object_type_id'])
                                    <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Группа объектов</label>
                        <select class="form-control" name="object_group_id" id="object_group_id">
                            <option value="-1">{{ trans('global.all') }}</option>
                            @foreach($object_groups as $key => $item)
                                @if(isset($_GET['object_group_id']) && $item->id == $_GET['object_group_id'])
                                    <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Категория фонда</label>
                        <select class="form-control" name="fond_cat_id" id="fond_cat_id">
                            <option value="-1">{{ trans('global.all') }}</option>
                            @foreach($fond_categories as $key => $item)
                                @if(isset($_GET['fond_cat_id']) && $item->id == $_GET['fond_cat_id'])
                                    <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                      </div>
                    </div>
                    </div>
                    <div class='row'>
                      <div class="col-sm-5"></div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="ПОИСК">
                        </div>
                    </div>
                    </div>

                </form>
              </div>
              <!-- /.card-body -->
            </div>

<div class="card">
    <div class="card-header">
        {{ trans('cruds.document.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <table class=" table table-bordered table-striped table-hover ajaxTable datatable datatable-Document">
            <thead>
                <tr>
                    <th>
                        &nbsp;
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.id') }}
                    </th>
					<th>
                        Инвент. номер
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.barcode') }}
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.city') }}
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.district') }}
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.cadastrnum') }}
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.address') }}
                    </th>
                    <th>
                        ФИО
                    </th>
                    <th>
                        ИИН
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.litera') }}
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.object_name') }}
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.celevoe_naznachenie') }}
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.fond_category') }}
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.object_type') }}
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.object_group') }}
                    </th>
                    <th>
                        {{ trans('cruds.document.fields.is_secret') }}
                    </th>
                </tr>
            </thead>
        </table>
    </div>
</div>

<style>
	/*
    .dt-buttons{
        display: none;
    }
    .dataTables_length{
        display: none;
    }
    .dataTables_filter{
        display: none;
    }*/
</style>

@endsection
@section('scripts')
@parent
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>--}}
<script>
    // $(function(){
    //     //2. Получить элемент, к которому необходимо добавить маску
    //     $("#cad_num").mask("99-999-999-999");
    // });
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('document_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}';
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.documents.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).data(), function (entry) {
          return entry.id
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

$(document).ready( function() {
  let dtOverrideGlobals = {
    buttons: dtButtons,
    processing: true,
    serverSide: true,
    retrieve: true,
    aaSorting: [],
    ajax: { 
        url: "/admin/documents/ajax-docs",
		'type': 'POST',
		'headers': {
			'X-CSRF-TOKEN': '{{ csrf_token() }}'
		},
        data: function (d){
            d.city_id = $("#city_id").val();
            d.district_id = $("#district_id").val();
            d.cad_num = $("#cad_num").val();
            d.address = $("#address").val();
            d.litera = $("#litera").val();
            d.goal_id = $("#goal_id").val();
            d.object_type_id = $("#object_type_id").val();
            d.object_group_id = $("#object_group_id").val();
            d.fond_cat_id = $("#fond_cat_id").val();
            d.wtrix = $("#wtrix").val();
            d.invent_num = $("#invent_num").val();
            d.iin = $("#iin").val();
        }
    },
    columns: [
    { data: 'actions', name: '{{ trans('global.actions') }}' },
{ data: 'id', name: 'id' },
{ data: 'invent_num', name: 'invent_num' },
{ data: 'barcode', name: 'barcode' },
{ data: 'city_name', name: 'city.name' },
{ data: 'district_name', name: 'district.name' },
{ data: 'cadastrnum', name: 'cadastrnum' },
{ data: 'address', name: 'address' },
{ data: 'fio', name: 'fio' },
{ data: 'iin', name: 'iin' },
{ data: 'litera', name: 'litera' },
{ data: 'object_name', name: 'object_name' },
{ data: 'celevoe_naznachenie_name', name: 'celevoe_naznachenie.name' },
{ data: 'fond_category_name', name: 'fond_category.name' },
{ data: 'object_type_name', name: 'object_type.name' },
{ data: 'object_group_name', name: 'object_group.name' },
{ data: 'is_secret', name: 'is_secret' }
    ],
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 7,
  };
  let table = $('.datatable-Document').DataTable(dtOverrideGlobals);
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });
  $('.datatable thead').on('input', '.search', function () {
      let strict = $(this).attr('strict') || false
      let value = strict && this.value ? "^" + this.value + "$" : this.value
      table
        .column($(this).parent().index())
        .search(value, strict)
        .draw()
  });
});
});

</script>
@endsection