@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} {{ trans('cruds.document.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.documents.update", [$document->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="barcode">{{ trans('cruds.document.fields.barcode') }}</label>
                <input class="form-control {{ $errors->has('barcode') ? 'is-invalid' : '' }}" type="text" name="barcode" id="barcode" value="{{ old('barcode', $document->barcode) }}" readonly="readonly">
                @if($errors->has('barcode'))
                    <span class="text-danger">{{ $errors->first('barcode') }}</span>
                @endif
                <?php echo '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($document->barcode, 'C39',1.5,70,array(1,1,1), true) . '" alt="barcode"   />'; ?>
                            <br>
                            <a href="data:image/png;base64,<?php echo DNS1D::getBarcodePNG($document->barcode, 'C39',1.5,70,array(1,1,1), true); ?>" download="{{$document->barcode}}">Скачать</a>
            </div>
			<div class="form-group">
                <label for="invent_num">Инвентарный номер</label>
                <input class="form-control {{ $errors->has('invent_num') ? 'is-invalid' : '' }}" type="text" name="invent_num" id="invent_num" value="{{ old('invent_num', $document->invent_num) }}">
                @if($errors->has('invent_num'))
                    <span class="text-danger">{{ $errors->first('invent_num') }}</span>
                @endif
                <span class="help-block"></span>
            </div>
            <div class="form-group">
                <label for="inv_nom_reg_del">Инвентарный № рег. дела</label>
                <input class="form-control {{ $errors->has('inv_nom_reg_del') ? 'is-invalid' : '' }}" type="text" name="inv_nom_reg_del" id="inv_nom_reg_del" value="{{ old('inv_nom_reg_del', $document->inv_nom_reg_del) }}">
                @if($errors->has('inv_nom_reg_del'))
                    <span class="text-danger">{{ $errors->first('inv_nom_reg_del') }}</span>
                @endif
                <span class="help-block"></span>
            </div>
            <div class="form-group">
                <label for="city_id">{{ trans('cruds.document.fields.city') }}</label>
                <select class="form-control select2 {{ $errors->has('city') ? 'is-invalid' : '' }}" name="city_id" id="city_id">
                    @foreach($cities as $id => $city)
                        <option value="{{ $id }}" {{ (old('city_id') ? old('city_id') : $document->city->id ?? '') == $id ? 'selected' : '' }}>{{ $city }}</option>
                    @endforeach
                </select>
                @if($errors->has('city'))
                    <span class="text-danger">{{ $errors->first('city') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.city_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="district_id">{{ trans('cruds.document.fields.district') }}</label>
                <select class="form-control select2 {{ $errors->has('district') ? 'is-invalid' : '' }}" name="district_id" id="district_id">
                    @foreach($districts as $id => $district)
                        <option value="{{ $id }}" {{ (old('district_id') ? old('district_id') : $document->district->id ?? '') == $id ? 'selected' : '' }}>{{ $district }}</option>
                    @endforeach
                </select>
                @if($errors->has('district'))
                    <span class="text-danger">{{ $errors->first('district') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.district_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="cadastrnum">{{ trans('cruds.document.fields.cadastrnum') }}</label>
                <input class="form-control {{ $errors->has('cadastrnum') ? 'is-invalid' : '' }}" type="text" name="cadastrnum" id="cadastrnum" value="{{ old('cadastrnum', $document->cadastrnum) }}">
                @if($errors->has('cadastrnum'))
                    <span class="text-danger">{{ $errors->first('cadastrnum') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.cadastrnum_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="address">{{ trans('cruds.document.fields.address') }}</label>
                <input class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" type="text" name="address" id="address" value="{{ old('address', $document->address) }}">
                @if($errors->has('address'))
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.address_helper') }}</span>
            </div>
             <div class="form-group">
                <label for="fio">ФИО</label>
                <input class="form-control {{ $errors->has('fio') ? 'is-invalid' : '' }}" type="text" name="fio" id="fio" value="{{ old('fio', $document->fio) }}">
                @if($errors->has('fio'))
                    <span class="text-danger">{{ $errors->first('fio') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label for="iin">ИИН</label>
                <input class="form-control {{ $errors->has('iin') ? 'is-invalid' : '' }}" type="text" name="iin" id="iin" value="{{ old('iin', $document->iin) }}">
                @if($errors->has('iin'))
                    <span class="text-danger">{{ $errors->first('iin') }}</span>
                @endif
                <span class="help-block">ИИН из 12 цифр</span>
            </div>
            <div class="form-group">
                <label for="litera">{{ trans('cruds.document.fields.litera') }}</label>
                <input class="form-control {{ $errors->has('litera') ? 'is-invalid' : '' }}" type="text" name="litera" id="litera" value="{{ old('litera', $document->litera) }}">
                @if($errors->has('litera'))
                    <span class="text-danger">{{ $errors->first('litera') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.litera_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="object_name">{{ trans('cruds.document.fields.object_name') }}</label>
                <input class="form-control {{ $errors->has('object_name') ? 'is-invalid' : '' }}" type="text" name="object_name" id="object_name" value="{{ old('object_name', $document->object_name) }}">
                @if($errors->has('object_name'))
                    <span class="text-danger">{{ $errors->first('object_name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.object_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="celevoe_naznachenie_id">{{ trans('cruds.document.fields.celevoe_naznachenie') }}</label>
                <select class="form-control select2 {{ $errors->has('celevoe_naznachenie') ? 'is-invalid' : '' }}" name="celevoe_naznachenie_id" id="celevoe_naznachenie_id">
                    @foreach($celevoe_naznachenies as $id => $celevoe_naznachenie)
                        <option value="{{ $id }}" {{ (old('celevoe_naznachenie_id') ? old('celevoe_naznachenie_id') : $document->celevoe_naznachenie->id ?? '') == $id ? 'selected' : '' }}>{{ $celevoe_naznachenie }}</option>
                    @endforeach
                </select>
                @if($errors->has('celevoe_naznachenie'))
                    <span class="text-danger">{{ $errors->first('celevoe_naznachenie') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.celevoe_naznachenie_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="fond_category_id">{{ trans('cruds.document.fields.fond_category') }}</label>
                <select class="form-control select2 {{ $errors->has('fond_category') ? 'is-invalid' : '' }}" name="fond_category_id" id="fond_category_id">
                    @foreach($fond_categories as $id => $fond_category)
                        <option value="{{ $id }}" {{ (old('fond_category_id') ? old('fond_category_id') : $document->fond_category->id ?? '') == $id ? 'selected' : '' }}>{{ $fond_category }}</option>
                    @endforeach
                </select>
                @if($errors->has('fond_category'))
                    <span class="text-danger">{{ $errors->first('fond_category') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.fond_category_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="object_type_id">{{ trans('cruds.document.fields.object_type') }}</label>
                <select class="form-control select2 {{ $errors->has('object_type') ? 'is-invalid' : '' }}" name="object_type_id" id="object_type_id">
                    @foreach($object_types as $id => $object_type)
                        <option value="{{ $id }}" {{ (old('object_type_id') ? old('object_type_id') : $document->object_type->id ?? '') == $id ? 'selected' : '' }}>{{ $object_type }}</option>
                    @endforeach
                </select>
                @if($errors->has('object_type'))
                    <span class="text-danger">{{ $errors->first('object_type') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.object_type_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="object_group_id">{{ trans('cruds.document.fields.object_group') }}</label>
                <select class="form-control select2 {{ $errors->has('object_group') ? 'is-invalid' : '' }}" name="object_group_id" id="object_group_id">
                    @foreach($object_groups as $id => $object_group)
                        <option value="{{ $id }}" {{ (old('object_group_id') ? old('object_group_id') : $document->object_group->id ?? '') == $id ? 'selected' : '' }}>{{ $object_group }}</option>
                    @endforeach
                </select>
                @if($errors->has('object_group'))
                    <span class="text-danger">{{ $errors->first('object_group') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.object_group_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.document.fields.is_secret') }}</label>
                <select class="form-control {{ $errors->has('is_secret') ? 'is-invalid' : '' }}" name="is_secret" id="is_secret" required>
                    <option value disabled {{ old('is_secret', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Document::IS_SECRET_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('is_secret', $document->is_secret) === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('is_secret'))
                    <span class="text-danger">{{ $errors->first('is_secret') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.is_secret_helper') }}</span>
            </div>
            <!--
            <div class="form-group">
                <label for="scaned_docs">{{ trans('cruds.document.fields.scaned_docs') }}</label>
                <div class="needsclick dropzone {{ $errors->has('scaned_docs') ? 'is-invalid' : '' }}" id="scaned_docs-dropzone">
                </div>
                @if($errors->has('scaned_docs'))
                    <span class="text-danger">{{ $errors->first('scaned_docs') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.scaned_docs_helper') }}</span>
            </div>!-->
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>

<div class="card">
    <div class="card-header">
        Инвентарные дела
    </div>
    <div class="card-body">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td width=50%>
                            <form action="/admin/add_file" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="doc_number_old" value="{{$document->doc_number_old}}">
                                    <input type="hidden" name="doc_id" value="{{$document->id}}">
                                    <input type="file" name="scaned_doc_sheet" class="col-sm-9 form-control">
                                    <input type="submit" class="col-sm-3 btn btn-primary" value="Загрузить">
                                </div>
                            </form>
                        </td>
                        <td width=50%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50% !important;">
                            <script src="/js/tiff.min.js"></script>
                            <style>
                                .doc_for canvas{
                                    width:600px !important;
                                }
                            </style>
                            <nav class="mt-2">
                            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            @foreach($document->files($document->doc_number_old) as $file)
                            <li class="nav-item">
                                <a href="/admin/delete_file/{{$file->id}}/{{$document->doc_number_old}}"><button class="btn-xs btn-danger">Удалить</button></a>
                                <?php
                                    $two_parts = explode(".", $file->filename);
                                    $extension = $two_parts[1];
                                ?>
                                @if($extension == "pdf")
                                    <a style="float:right;" href="#/" onClick="show_pdf('/storage/documents/{{$document->doc_number_old}}/{{$file->filename}}');">{{$file->filename}}</a>
                                @else
                                    <a style="float:right;" href="#/" onClick="show_file('/storage/documents/{{$document->doc_number_old}}/{{$file->filename}}');">{{$file->filename}}</a>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </nav>
                        </td>
                        <td class="col-sm-6">
                            <div id="doc" class="doc_for"></div>
                        </td>
                    </tr>
                </tbody>
            </table>
    </div>
</div>

<div class="card">
    <div class="card-header">
        Регистрационные дела РН
    </div>
    <div class="card-body">
            <table class="table table-striped">
                <tbody>
                    <tr>
                        <td width=50%>
                            <form action="/admin/add_file_jur" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="doc_number_old" value="{{$document->doc_number_old}}">
                                    <input type="hidden" name="doc_id" value="{{$document->id}}">
                                    <input type="file" name="scaned_doc_sheet" class="col-sm-9 form-control">
                                    <input type="submit" class="col-sm-3 btn btn-primary" value="Загрузить">
                                </div>
                            </form>
                        </td>
                        <td width=50%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50% !important;">
                            <script src="/js/tiff.min.js"></script>
                            <style>
                                .doc_for canvas{
                                    width:600px !important;
                                }
                            </style>
                            <nav class="mt-2">
                            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            @foreach($document->jur_files($document->doc_number_old) as $file)
                            <li class="nav-item">
                                <a href="/admin/delete_jur_file/{{$file->id}}/{{$document->doc_number_old}}"><button class="btn-xs btn-danger">Удалить</button></a>
                                <?php
                                    $two_parts = explode(".", $file->filename);
                                    $extension = $two_parts[1];
                                ?>
                                @if($extension == "pdf")
                                    <a style="float:right;" href="#/" onClick="show_pdf2('/storage/documents/{{$document->doc_number_old}}/{{$file->filename}}');">{{$file->filename}}</a>
                                @else
                                    <a style="float:right;" href="#/" onClick="show_file2('/storage/documents/{{$document->doc_number_old}}/{{$file->filename}}');">{{$file->filename}}</a>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                    </nav>
                        </td>
                        <td class="col-sm-6">
                            <div id="doc2" class="doc_for"></div>
                        </td>
                    </tr>
                </tbody>
            </table>
    </div>
</div>

<div class="card">
    <div class="card-header">
        Регистрационные дела ЮЛ
    </div>
    <div class="card-body">
        <table class="table table-striped">
            <tbody>
            <tr>
                <td width=50%>
                    <form action="/admin/add_file_jur2" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            {{ csrf_field() }}
                            <input type="hidden" name="doc_number_old" value="{{$document->doc_number_old}}">
                            <input type="hidden" name="doc_id" value="{{$document->id}}">
                            <input type="file" name="scaned_doc_sheet" class="col-sm-9 form-control">
                            <input type="submit" class="col-sm-3 btn btn-primary" value="Загрузить">
                        </div>
                    </form>
                </td>
                <td width=50%>
                </td>
            </tr>
            <tr>
                <td style="width:50% !important;">
                    <script src="/js/tiff.min.js"></script>
                    <style>
                        .doc_for canvas{
                            width:600px !important;
                        }
                    </style>
                    <nav class="mt-2">
                        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                            @foreach($document->jur_files2($document->doc_number_old) as $file)
                                <li class="nav-item">
                                    <a href="/admin/delete_jur_file2/{{$file->id}}/{{$document->doc_number_old}}"><button class="btn-xs btn-danger">Удалить</button></a>
                                    <?php
                                    $two_parts = explode(".", $file->filename);
                                    $extension = $two_parts[1];
                                    ?>
                                    @if($extension == "pdf")
                                        <a style="float:right;" href="#/" onClick="show_pdf2('/storage/documents/{{$document->doc_number_old}}/{{$file->filename}}');">{{$file->filename}}</a>
                                    @else
                                        <a style="float:right;" href="#/" onClick="show_file2('/storage/documents/{{$document->doc_number_old}}/{{$file->filename}}');">{{$file->filename}}</a>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                </td>
                <td class="col-sm-6">
                    <div id="doc2" class="doc_for"></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</div>

<h4>История документа</h4>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Автор</th>
                        <th>Действие</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($journal_actions as $action)
                    <tr>
                        <td>{{$action->user($action->author_id)}}</td>
                        <td>{!!$action->action!!}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>


@endsection

@section('scripts')
<script>
     function show_file(url){
                                    $("#doc").empty();
                                    $("#doc").html("<a href='"+url+"'>Скачать полный файл</a><br><br>");
                                    var xhr = new XMLHttpRequest();
                                    xhr.responseType = 'arraybuffer';
                                    xhr.open('GET', url);
                                    xhr.onload = function (e) {
                                        var tiff = new Tiff({buffer: xhr.response});
                                        var canvas = tiff.toCanvas();
                                        document.getElementById("doc").append(canvas);
                                        $([document.documentElement, document.body]).animate({
                                            scrollTop: $("#doc").offset().top
                                        }, 1000);
                                    };
                                    xhr.send();
        }
        function show_pdf(url){
                $("#doc").empty();
                $("#doc").html("<a target='_blank' href='"+url+"'>Скачать полный файл</a><br><br>")
                $("#doc").append('<div><object data="'+url+'" type="application/pdf" width="600" height=600px></object></div>');
                $([document.documentElement, document.body]).animate({scrollTop: $("#doc").offset().top}, 1000);

        }
		function show_file2(url){
                                    $("#doc2").empty();
                                    $("#doc2").html("<a href='"+url+"'>Скачать полный файл</a><br><br>");
                                    var xhr = new XMLHttpRequest();
                                    xhr.responseType = 'arraybuffer';
                                    xhr.open('GET', url);
                                    xhr.onload = function (e) {
                                        var tiff = new Tiff({buffer: xhr.response});
                                        var canvas = tiff.toCanvas();
                                        document.getElementById("doc2").append(canvas);
                                        $([document.documentElement, document.body]).animate({
                                            scrollTop: $("#doc2").offset().top
                                        }, 1000);
                                    };
                                    xhr.send();
        }
        function show_pdf2(url){
                $("#doc2").empty();
                $("#doc2").html("<a target='_blank' href='"+url+"'>Скачать полный файл</a><br><br>")
                $("#doc2").append('<div><object data="'+url+'" type="application/pdf" width="600" height=600px></object></div>');
                $([document.documentElement, document.body]).animate({scrollTop: $("#doc2").offset().top}, 1000);

        }
</script>
@endsection