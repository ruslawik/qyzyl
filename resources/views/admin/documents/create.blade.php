@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.document.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.documents.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="invent_num">Инвентарный номер</label>
                <input class="form-control {{ $errors->has('invent_num') ? 'is-invalid' : '' }}" type="text" name="invent_num" id="invent_num" value="{{ old('invent_num', '') }}">
                @if($errors->has('invent_num'))
                    <span class="text-danger">{{ $errors->first('invent_num') }}</span>
                @endif
                <span class="help-block"></span>
            </div>
            <div class="form-group">
                <label for="inv_nom_reg_del">Инвентарный № рег. дела</label>
                <input class="form-control {{ $errors->has('inv_nom_reg_del') ? 'is-invalid' : '' }}" type="text" name="inv_nom_reg_del" id="inv_nom_reg_del" value="{{ old('inv_nom_reg_del', '') }}">
                @if($errors->has('inv_nom_reg_del'))
                    <span class="text-danger">{{ $errors->first('inv_nom_reg_del') }}</span>
                @endif
                <span class="help-block"></span>
            </div>
            <div class="form-group">
                <label for="city_id">{{ trans('cruds.document.fields.city') }}</label>
                <select class="form-control select2 {{ $errors->has('city') ? 'is-invalid' : '' }}" name="city_id" id="city_id">
                    @foreach($cities as $id => $city)
                        <option value="{{ $id }}" {{ old('city_id') == $id ? 'selected' : '' }}>{{ $city }}</option>
                    @endforeach
                </select>
                @if($errors->has('city'))
                    <span class="text-danger">{{ $errors->first('city') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.city_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="district_id">{{ trans('cruds.document.fields.district') }}</label>
                <select class="form-control select2 {{ $errors->has('district') ? 'is-invalid' : '' }}" name="district_id" id="district_id">
                    @foreach($districts as $id => $district)
                        <option value="{{ $id }}" {{ old('district_id') == $id ? 'selected' : '' }}>{{ $district }}</option>
                    @endforeach
                </select>
                @if($errors->has('district'))
                    <span class="text-danger">{{ $errors->first('district') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.district_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="cadastrnum">{{ trans('cruds.document.fields.cadastrnum') }}</label>
                <input class="form-control {{ $errors->has('cadastrnum') ? 'is-invalid' : '' }}" type="text" name="cadastrnum" id="cadastrnum" value="{{ old('cadastrnum', '') }}">
                @if($errors->has('cadastrnum'))
                    <span class="text-danger">{{ $errors->first('cadastrnum') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.cadastrnum_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="address">{{ trans('cruds.document.fields.address') }}</label>
                <input class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" type="text" name="address" id="address" value="{{ old('address', '') }}">
                @if($errors->has('address'))
                    <span class="text-danger">{{ $errors->first('address') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.address_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="fio">ФИО</label>
                <input class="form-control {{ $errors->has('fio') ? 'is-invalid' : '' }}" type="text" name="fio" id="fio" value="{{ old('fio', '') }}">
                @if($errors->has('fio'))
                    <span class="text-danger">{{ $errors->first('fio') }}</span>
                @endif
            </div>
            <div class="form-group">
                <label for="iin">ИИН</label>
                <input class="form-control {{ $errors->has('iin') ? 'is-invalid' : '' }}" type="text" name="iin" id="iin" value="{{ old('iin', '') }}">
                @if($errors->has('iin'))
                    <span class="text-danger">{{ $errors->first('iin') }}</span>
                @endif
                <span class="help-block">ИИН из 12 цифр</span>
            </div>
            <div class="form-group">
                <label for="litera">{{ trans('cruds.document.fields.litera') }}</label>
                <input class="form-control {{ $errors->has('litera') ? 'is-invalid' : '' }}" type="text" name="litera" id="litera" value="{{ old('litera', '') }}">
                @if($errors->has('litera'))
                    <span class="text-danger">{{ $errors->first('litera') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.litera_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="object_name">{{ trans('cruds.document.fields.object_name') }}</label>
                <input class="form-control {{ $errors->has('object_name') ? 'is-invalid' : '' }}" type="text" name="object_name" id="object_name" value="{{ old('object_name', '') }}">
                @if($errors->has('object_name'))
                    <span class="text-danger">{{ $errors->first('object_name') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.object_name_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="celevoe_naznachenie_id">{{ trans('cruds.document.fields.celevoe_naznachenie') }}</label>
                <select class="form-control select2 {{ $errors->has('celevoe_naznachenie') ? 'is-invalid' : '' }}" name="celevoe_naznachenie_id" id="celevoe_naznachenie_id">
                    @foreach($celevoe_naznachenies as $id => $celevoe_naznachenie)
                        <option value="{{ $id }}" {{ old('celevoe_naznachenie_id') == $id ? 'selected' : '' }}>{{ $celevoe_naznachenie }}</option>
                    @endforeach
                </select>
                @if($errors->has('celevoe_naznachenie'))
                    <span class="text-danger">{{ $errors->first('celevoe_naznachenie') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.celevoe_naznachenie_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="fond_category_id">{{ trans('cruds.document.fields.fond_category') }}</label>
                <select class="form-control select2 {{ $errors->has('fond_category') ? 'is-invalid' : '' }}" name="fond_category_id" id="fond_category_id">
                    @foreach($fond_categories as $id => $fond_category)
                        <option value="{{ $id }}" {{ old('fond_category_id') == $id ? 'selected' : '' }}>{{ $fond_category }}</option>
                    @endforeach
                </select>
                @if($errors->has('fond_category'))
                    <span class="text-danger">{{ $errors->first('fond_category') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.fond_category_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="object_type_id">{{ trans('cruds.document.fields.object_type') }}</label>
                <select class="form-control select2 {{ $errors->has('object_type') ? 'is-invalid' : '' }}" name="object_type_id" id="object_type_id">
                    @foreach($object_types as $id => $object_type)
                        <option value="{{ $id }}" {{ old('object_type_id') == $id ? 'selected' : '' }}>{{ $object_type }}</option>
                    @endforeach
                </select>
                @if($errors->has('object_type'))
                    <span class="text-danger">{{ $errors->first('object_type') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.object_type_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="object_group_id">{{ trans('cruds.document.fields.object_group') }}</label>
                <select class="form-control select2 {{ $errors->has('object_group') ? 'is-invalid' : '' }}" name="object_group_id" id="object_group_id">
                    @foreach($object_groups as $id => $object_group)
                        <option value="{{ $id }}" {{ old('object_group_id') == $id ? 'selected' : '' }}>{{ $object_group }}</option>
                    @endforeach
                </select>
                @if($errors->has('object_group'))
                    <span class="text-danger">{{ $errors->first('object_group') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.object_group_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required">{{ trans('cruds.document.fields.is_secret') }}</label>
                <select class="form-control {{ $errors->has('is_secret') ? 'is-invalid' : '' }}" name="is_secret" id="is_secret" required>
                    <option value disabled {{ old('is_secret', null) === null ? 'selected' : '' }}>{{ trans('global.pleaseSelect') }}</option>
                    @foreach(App\Models\Document::IS_SECRET_SELECT as $key => $label)
                        <option value="{{ $key }}" {{ old('is_secret', 'no') === (string) $key ? 'selected' : '' }}>{{ $label }}</option>
                    @endforeach
                </select>
                @if($errors->has('is_secret'))
                    <span class="text-danger">{{ $errors->first('is_secret') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.is_secret_helper') }}</span>
            </div>
            <!--
            <div class="form-group">
                <label for="scaned_docs">{{ trans('cruds.document.fields.scaned_docs') }}</label>
                <div class="needsclick dropzone {{ $errors->has('scaned_docs') ? 'is-invalid' : '' }}" id="scaned_docs-dropzone">
                </div>
                @if($errors->has('scaned_docs'))
                    <span class="text-danger">{{ $errors->first('scaned_docs') }}</span>
                @endif
                <span class="help-block">{{ trans('cruds.document.fields.scaned_docs_helper') }}</span>
            </div>!-->
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    
</script>
@endsection