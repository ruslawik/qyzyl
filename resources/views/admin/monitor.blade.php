@extends('layouts.admin')
@section('content')

<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Мониторинг и формирование отчетов</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form role="form" method="GET" action="/admin/monitor">
                  <div class="row">
                    <div class="col-sm-2">
                      <!-- text input -->
                      <div class="form-group">
                        <label>Город</label>
                        <select class="form-control" name="city_id" id="city_id">
                            <option value="-1">{{ trans('global.all') }}</option>
                            @foreach($cities as $key => $item)
                                @if(isset($_GET['city_id']) && $item->id == $_GET['city_id'])
                                    <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <!-- text input -->
                      <div class="form-group">
                        <label>Район</label>
                        <select class="form-control" name="district_id" id="district_id">
                            <option value="-1">{{ trans('global.all') }}</option>
                            @foreach($districts as $key => $item)
                                @if(isset($_GET['district_id']) && $item->id == $_GET['district_id'])
                                    <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Целевое назначение</label>
                        <select class="form-control" name="goal_id" id="goal_id">
                            <option value="-1">{{ trans('global.all') }}</option>
                            @foreach($goals as $key => $item)
                                @if(isset($_GET['goal_id']) && $item->id == $_GET['goal_id'])
                                    <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Тип объекта</label>
                        <select class="form-control" name="object_type_id" id="object_type_id">
                            <option value="-1">{{ trans('global.all') }}</option>
                            @foreach($object_types as $key => $item)
                                @if(isset($_GET['object_type_id']) && $item->id == $_GET['object_type_id'])
                                    <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Группа объектов</label>
                        <select class="form-control" name="object_group_id" id="object_group_id">
                            <option value="-1">{{ trans('global.all') }}</option>
                            @foreach($object_groups as $key => $item)
                                @if(isset($_GET['object_group_id']) && $item->id == $_GET['object_group_id'])
                                    <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Категория фонда</label>
                        <select class="form-control" name="fond_cat_id" id="fond_cat_id">
                            <option value="-1">{{ trans('global.all') }}</option>
                            @foreach($fond_categories as $key => $item)
                                @if(isset($_GET['fond_cat_id']) && $item->id == $_GET['fond_cat_id'])
                                    <option value="{{ $item->id }}" selected="selected">{{ $item->name }}</option>
                                @else
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Дата от</label>
                        <input type="date" class="form-control" name="date_from" @if(isset($_GET['date_from'])) value="{{$_GET['date_from']}}" @endif>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Дата до</label>
                        <input type="date" class="form-control" name="date_to" @if(isset($_GET['date_to'])) value="{{$_GET['date_to']}}" @endif>
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <label>&nbsp;</label>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Показать статистику">
                        </div>
                    </div>
                    </div>

                </form>
                <h4>Добавлено документов: </h4>
                <table class="table table-striped" id="monitor">
                  <thead>
                    <th>Дата</th>
                    <th>Документы</th>
                    <th>Инвентарные дела</th>
                    <th>Регистрационные дела (РН)</th>
                    <th>Регистрационные дела (ЮЛ)</th>
                  </thead>
                @foreach($result as $day)
                 <tr>

                    <td>{{$day->date}}</td>

                     @if( isset($day->total) )
                        <td>{{$day->total}}</td>
                     @else
                         <td>0</td>
                     @endif

                     @if( isset($day->total_files) )
                        <td>{{ $day->total_files }}</td>
                     @else
                         <td>{{ \App\Models\Files::where('created_at', $day->date)->count() }}</td>
                     @endif


                     @if( isset($day->total_jur) )
                         <td>{{ $day->total_jur }}</td>
                     @else
                         <td>{{ \App\Models\JurFiles::where('created_at', $day->date)->count() }}</td>
                     @endif

                     @if( isset($day->total_jur2) )
                         <td>{{ $day->total_jur2 }}</td>
                     @else
                         <td>{{ \App\Models\JurFiles2::where('created_at', $day->date)->count() }}</td>
                     @endif

                 </tr>
                @endforeach
                </table>

              </div>
              <!-- /.card-body -->
            </div>
@endsection
@section('scripts')
    <script>
        $("#monitor").DataTable( {
    dom: 'Bfrtip',
    buttons: [
        'excel', 'pdf'
    ]
} );
    </script>

@endsection
