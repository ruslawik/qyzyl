<?php

return [
    'userManagement' => [
        'title'          => 'Учетные записи',
        'title_singular' => 'Учетная запись',
    ],
    'permission'     => [
        'title'          => 'Разрешения',
        'title_singular' => 'Разрешение',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'title'             => 'Title',
            'title_helper'      => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
        ],
    ],
    'role'           => [
        'title'          => 'Роли',
        'title_singular' => 'Роль',
        'fields'         => [
            'id'                 => 'ID',
            'id_helper'          => ' ',
            'title'              => 'Title',
            'title_helper'       => ' ',
            'permissions'        => 'Permissions',
            'permissions_helper' => ' ',
            'created_at'         => 'Created at',
            'created_at_helper'  => ' ',
            'updated_at'         => 'Updated at',
            'updated_at_helper'  => ' ',
            'deleted_at'         => 'Deleted at',
            'deleted_at_helper'  => ' ',
        ],
    ],
    'user'           => [
        'title'          => 'Пользователи',
        'title_singular' => 'Пользователь',
        'fields'         => [
            'id'                       => 'ID',
            'id_helper'                => ' ',
            'name'                     => 'Name',
            'name_helper'              => ' ',
            'email'                    => 'Email',
            'email_helper'             => ' ',
            'email_verified_at'        => 'Email verified at',
            'email_verified_at_helper' => ' ',
            'password'                 => 'Password',
            'password_helper'          => ' ',
            'roles'                    => 'Roles',
            'roles_helper'             => ' ',
            'remember_token'           => 'Remember Token',
            'remember_token_helper'    => ' ',
            'created_at'               => 'Created at',
            'created_at_helper'        => ' ',
            'updated_at'               => 'Updated at',
            'updated_at_helper'        => ' ',
            'deleted_at'               => 'Deleted at',
            'deleted_at_helper'        => ' ',
        ],
    ],
    'spravochniki'   => [
        'title'          => 'Справочники',
        'title_singular' => 'Справочники',
    ],
    'city'           => [
        'title'          => 'Города',
        'title_singular' => 'Города',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'name'              => 'Название',
            'name_helper'       => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
        ],
    ],
    'district'       => [
        'title'          => 'Районы',
        'title_singular' => 'Районы',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'city'              => 'Город',
            'city_helper'       => ' ',
            'name'              => 'Название района',
            'name_helper'       => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
        ],
    ],
    'goal'           => [
        'title'          => 'Целевое назначение',
        'title_singular' => 'Целевое назначение',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'name'              => 'Целевое назначение',
            'name_helper'       => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
        ],
    ],
    'fondCategory'   => [
        'title'          => 'Категория фонда',
        'title_singular' => 'Категория фонда',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'name'              => 'Категория фонда',
            'name_helper'       => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
        ],
    ],
    'objectGroup'    => [
        'title'          => 'Группа объектов',
        'title_singular' => 'Группа объектов',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'name'              => 'Группа объектов',
            'name_helper'       => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
        ],
    ],
    'objectType'     => [
        'title'          => 'Тип объекта',
        'title_singular' => 'Тип объекта',
        'fields'         => [
            'id'                => 'ID',
            'id_helper'         => ' ',
            'name'              => 'Тип объекта',
            'name_helper'       => ' ',
            'created_at'        => 'Created at',
            'created_at_helper' => ' ',
            'updated_at'        => 'Updated at',
            'updated_at_helper' => ' ',
            'deleted_at'        => 'Deleted at',
            'deleted_at_helper' => ' ',
        ],
    ],
    'document'       => [
        'title'          => 'Документы',
        'title_singular' => 'Документы',
        'fields'         => [
            'id'                         => 'ID',
            'id_helper'                  => ' ',
            'barcode'                    => 'Номер штрих кода',
            'barcode_helper'             => ' ',
            'city'                       => 'Город',
            'city_helper'                => ' ',
            'district'                   => 'Район',
            'district_helper'            => ' ',
            'cadastrnum'                 => 'Кадастровый номер',
            'cadastrnum_helper'          => ' ',
            'address'                    => 'Адрес',
            'address_helper'             => ' ',
            'litera'                     => 'Литера',
            'litera_helper'              => ' ',
            'object_name'                => 'Наименование объекта',
            'object_name_helper'         => ' ',
            'celevoe_naznachenie'        => 'Целевое назначение',
            'celevoe_naznachenie_helper' => ' ',
            'fond_category'              => 'Категория фонда',
            'fond_category_helper'       => ' ',
            'object_type'                => 'Тип объекта',
            'object_type_helper'         => ' ',
            'object_group'               => 'Группа объектов',
            'object_group_helper'        => ' ',
            'barcodeint'                 => 'Barcodeint',
            'barcodeint_helper'          => ' ',
            'doc_number_old'             => 'Номер документа со старой базы',
            'doc_number_old_helper'      => ' ',
            'sheets_in_doc'              => 'Листов в документе',
            'sheets_in_doc_helper'       => ' ',
            'created_at'                 => 'Created at',
            'created_at_helper'          => ' ',
            'updated_at'                 => 'Updated at',
            'updated_at_helper'          => ' ',
            'deleted_at'                 => 'Deleted at',
            'deleted_at_helper'          => ' ',
            'is_secret'                  => 'Документ ограниченного пользования',
            'is_secret_helper'           => ' ',
            'scaned_docs'                => 'Сканированные документы',
            'scaned_docs_helper'         => ' ',
        ],
    ],
];
