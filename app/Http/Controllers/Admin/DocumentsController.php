<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyDocumentRequest;
use App\Http\Requests\StoreDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Models\City;
use App\Models\District;
use App\Models\Document;
use App\Models\FondCategory;
use App\Models\Goal;
use App\Models\JurFiles2;
use App\Models\ObjectGroup;
use App\Models\ObjectType;
use Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Auth;
use App\Models\Files;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Journal;
use File;
use ZipArchive;
use App\Models\JurFiles;
use Carbon\Carbon;

class DocumentsController extends Controller
{
    use MediaUploadingTrait;

    public function downloadZip($doc_id)
    {
        $zip = new ZipArchive;

        $fileName = $doc_id.'.zip';

        $document = Document::where('id', $doc_id)->get();

        if ($zip->open(public_path($fileName), ZipArchive::CREATE) === TRUE)
        {
            $files = File::files(public_path('/storage/documents/'.$document[0]->doc_number_old));

            foreach ($files as $key => $value) {
                $relativeNameInZipFile = basename($value);
                $zip->addFile($value, $relativeNameInZipFile);
            }

            $zip->close();
        }

        return response()->download(public_path($fileName));
    }



    public function index(Request $request)
    {
        abort_if(Gate::denies('document_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cities          = City::get();
        $districts       = District::get();
        $goals           = Goal::get();
        $fond_categories = FondCategory::get();
        $object_types    = ObjectType::get();
        $object_groups   = ObjectGroup::get();

        return view('admin.documents.index', compact('cities', 'districts', 'goals', 'fond_categories', 'object_types', 'object_groups'));

    }

    public function ajax_docs (Request $request){

        $query = Document::query();

        if($request->has('city_id')){
            if($request['city_id'] != -1){
                $query->where('city_id', $request['city_id']);
            }
        }

        if($request->has('district_id')){
            if($request['district_id'] != -1){
                $query->where('district_id', $request['district_id']);
            }
        }

        if($request->has('goal_id')){
            if($request['goal_id'] != -1){
                $query->where('celevoe_naznachenie_id', $request['goal_id']);
            }
        }

        if($request->has('object_type_id')){
            if($request['object_type_id'] != -1){
                $query->where('object_type_id', $request['object_type_id']);
            }
        }

        if($request->has('object_group_id')){
            if($request['object_group_id'] != -1){
                $query->where('object_group_id', $request['object_group_id']);
            }
        }

        if($request->has('fond_cat_id')){
            if($request['fond_cat_id'] != -1){
                $query->where('fond_category_id', $request['fond_cat_id']);
            }
        }

        if($request->has('cad_num')){
            if(strlen($request['cad_num']) > 1){
                    $query->where(DB::raw("REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(cadastrnum, '-', ''), ',', ''), '.', ''), '/', ''), '*', '')"), 'LIKE', '%'.$request['cad_num'].'%');
            }
        }

        if($request->has('inv_nom_reg_del')){
            if(strlen($request['inv_nom_reg_del']) > 1){
                $query->where('inv_nom_reg_del', 'LIKE', '%'.$request['inv_nom_reg_del'].'%');
            }
        }

        if($request->has('address')){
            if(strlen($request['address']) > 1){
                $query->where('address', 'LIKE', '%'.$request['address'].'%');
            }
        }

        if($request->has('litera')){
            if(strlen($request['litera']) > 0){
                $query->where('litera', 'LIKE', '%'.$request['litera'].'%');
            }
        }
        if($request->has('invent_num')){
            if(strlen($request['invent_num']) > 0){
                $query->where('invent_num', 'LIKE', '%'.$request['invent_num'].'%');
            }
        }
        if($request->has('iin')){
            if(strlen($request['iin']) > 0){
                $query->where('iin', 'LIKE', '%'.$request['iin'].'%');
            }
        }

        if($request->has('wtrix')){
            if(strlen($request['wtrix']) > 1){
                $query->where('barcode', 'LIKE', '%'.$request['wtrix'].'%');
            }
        }
        if(Auth::user()->roles->contains(2)){
            $query->where('is_secret', 'no');
        }
        $query->with(['city', 'district', 'celevoe_naznachenie', 'fond_category', 'object_type', 'object_group'])->select(sprintf('%s.*', (new Document)->table));
        $table = Datatables::of($query);

        $table->addColumn('actions', '&nbsp;');
        $table->addColumn('placeholder', '&nbsp;');

        $table->editColumn('actions', function ($row) {
            $viewGate      = 'document_show';
            $editGate      = 'document_edit';
            $deleteGate    = 'document_delete';
            $crudRoutePart = 'documents';

            return view('partials.datatablesActions', compact(
                'viewGate',
                'editGate',
                'deleteGate',
                'crudRoutePart',
                'row'
            ));
        });

        $table->editColumn('id', function ($row) {
            return $row->id ? $row->id : "";
        });
        $table->editColumn('invent_num', function ($row) {
            return $row->invent_num ? $row->invent_num : "";
        });
        $table->editColumn('barcode', function ($row) {
            return $row->barcode ? $row->barcode : "";
        });
        $table->addColumn('city_name', function ($row) {
            return $row->city ? $row->city->name : '';
        });

        $table->addColumn('district_name', function ($row) {
            return $row->district ? $row->district->name : '';
        });

        $table->editColumn('cadastrnum', function ($row) {
            return $row->cadastrnum ? $row->cadastrnum : "";
        });
        $table->editColumn('address', function ($row) {
            return $row->address ? $row->address : "";
        });
        $table->addColumn('fio', function ($row) {
            return $row->fio ? $row->fio : '';
        });
        $table->addColumn('iin', function ($row) {
            return $row->iin ? $row->iin : '';
        });
        $table->editColumn('litera', function ($row) {
            return $row->litera ? $row->litera : "";
        });
        $table->editColumn('object_name', function ($row) {
            return $row->object_name ? $row->object_name : "";
        });
        $table->addColumn('celevoe_naznachenie_name', function ($row) {
            return $row->celevoe_naznachenie ? $row->celevoe_naznachenie->name : '';
        });

        $table->addColumn('fond_category_name', function ($row) {
            return $row->fond_category ? $row->fond_category->name : '';
        });

        $table->addColumn('object_type_name', function ($row) {
            return $row->object_type ? $row->object_type->name : '';
        });

        $table->addColumn('object_group_name', function ($row) {
            return $row->object_group ? $row->object_group->name : '';
        });

        $table->editColumn('is_secret', function ($row) {
            return $row->is_secret ? Document::IS_SECRET_SELECT[$row->is_secret] : '';
        });

        $table->rawColumns(['actions', 'placeholder', 'city', 'district', 'celevoe_naznachenie', 'fond_category', 'object_type', 'object_group']);

        return $table->make(true);

    }

    public function create()
    {
        abort_if(Gate::denies('document_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cities = City::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $districts = District::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $celevoe_naznachenies = Goal::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $fond_categories = FondCategory::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $object_types = ObjectType::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $object_groups = ObjectGroup::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        return view('admin.documents.create', compact('cities', 'districts', 'celevoe_naznachenies', 'fond_categories', 'object_types', 'object_groups'));
    }

    public function store(StoreDocumentRequest $request)
    {
        $document = Document::create($request->all());

        /*
        foreach ($request->input('scaned_docs', []) as $file) {
            $document->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('scaned_docs');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $document->id]);
        }*/
        $doc_id = $document->id;
        $letter = chr(rand(65,90));
        $document->where('id', $doc_id)->update(["barcode"=>$doc_id.$letter, "doc_number_old"=> $doc_id.$letter]);

        Journal::create(["author_id" => Auth::user()->id, "action" => Carbon::now('Asia/Qyzylorda')." - Документ создан", "document_id" => $doc_id]);

        return redirect('/admin/documents/'.$doc_id.'/edit')->with('message', 'Документ успешно добавлен!');
    }

    public function addFile(Request $r){

        $now_doc = Document::where('id', $r->input('doc_id'))->get();
        $file_num = $now_doc[0]->sheets_in_doc;
        $file_num = $file_num+1;

        $extension = $r->file('scaned_doc_sheet')->extension();

        $uploadedFile = $r->file('scaned_doc_sheet');
        $filename = $file_num.".".$extension;


        Storage::disk('local')->putFileAs(
            '/public/documents/'.$r->input('doc_number_old'),
            $uploadedFile,
            $filename
        );

        $file = new Files();
        $file->doc_id = $r->input('doc_number_old');
        $file->filename = $filename;
        $file->num = $file_num;
        $file->save();

        Document::where('id', $r->input('doc_id'))->update(["sheets_in_doc" => $file_num]);

        Journal::create(["author_id" => Auth::user()->id, "action" => Carbon::now('Asia/Qyzylorda')." - Добавлена сканированная страница в тех. часть - ".$filename, "document_id" => $r->input('doc_id')]);

        return back()->with('message', 'Файл успешно загружен!');
    }

    public function add_jur_File(Request $r){

        $now_doc = Document::where('id', $r->input('doc_id'))->get();
        $file_num = $now_doc[0]->sheets_in_doc;
        $file_num = $file_num+1;

        $extension = $r->file('scaned_doc_sheet')->extension();

        $uploadedFile = $r->file('scaned_doc_sheet');
        $filename = "jur_".$file_num.".".$extension;


        Storage::disk('local')->putFileAs(
            '/public/documents/'.$r->input('doc_number_old'),
            $uploadedFile,
            $filename
        );

        $file = new JurFiles();
        $file->doc_id = $r->input('doc_number_old');
        $file->filename = $filename;
        $file->num = $file_num;
        $file->save();

        Document::where('id', $r->input('doc_id'))->update(["sheets_in_doc" => $file_num]);

        Journal::create(["author_id" => Auth::user()->id, "action" => Carbon::now('Asia/Qyzylorda')." - Добавлена сканированная страница в юр. часть - ".$filename, "document_id" => $r->input('doc_id')]);

        return back()->with('message', 'Файл успешно загружен!');
    }

    public function add_jur_File2(Request $r){

        $now_doc = Document::where('id', $r->input('doc_id'))->get();
        $file_num = $now_doc[0]->sheets_in_doc;
        $file_num = $file_num+1;

        $extension = $r->file('scaned_doc_sheet')->extension();

        $uploadedFile = $r->file('scaned_doc_sheet');
        $filename = "jur2_".$file_num.".".$extension;


        Storage::disk('local')->putFileAs(
            '/public/documents/'.$r->input('doc_number_old'),
            $uploadedFile,
            $filename
        );

        $file = new JurFiles2();
        $file->doc_id = $r->input('doc_number_old');
        $file->filename = $filename;
        $file->num = $file_num;
        $file->save();

        Document::where('id', $r->input('doc_id'))->update(["sheets_in_doc" => $file_num]);

        Journal::create(["author_id" => Auth::user()->id, "action" => Carbon::now('Asia/Qyzylorda')." - Добавлена сканированная страница в юр. часть - ".$filename, "document_id" => $r->input('doc_id')]);

        return back()->with('message', 'Файл успешно загружен!');
    }

    public function deleteFile ($file_id, $doc_number){

        $file = Files::where('id', $file_id)->get();
        //Storage::delete('/public/documents/'.$doc_number."/".$file[0]->filename);
        Files::where('id', $file_id)->delete();
        $doc_data = Document::where('doc_number_old', $doc_number)->get();

        Journal::create(["author_id" => Auth::user()->id, "action" => Carbon::now('Asia/Qyzylorda')." - Удалена сканированная страница из инвентарных дел - <a target='_blank' href='/storage/documents/".$doc_number."/".$file[0]->filename."'>".$file[0]->filename."</a>", "document_id" => $doc_data[0]->id]);

        return back()->with('message', 'Файл успешно удален!');
    }

    public function deleteJurFile ($file_id, $doc_number){

        $file = JurFiles::where('id', $file_id)->get();
        //Storage::delete('/public/documents/'.$doc_number."/".$file[0]->filename);
        JurFiles::where('id', $file_id)->delete();
        $doc_data = Document::where('doc_number_old', $doc_number)->get();

        Journal::create(["author_id" => Auth::user()->id, "action" => Carbon::now('Asia/Qyzylorda')." - Удалена сканированная страница из регистрационных дел - <a target='_blank' href='/storage/documents/".$doc_number."/".$file[0]->filename."'>".$file[0]->filename."</a>", "document_id" => $doc_data[0]->id]);

        return back()->with('message', 'Файл успешно удален!');
    }

    public function deleteJurFile2 ($file_id, $doc_number){

        $file = JurFiles::where('id', $file_id)->get();
        //Storage::delete('/public/documents/'.$doc_number."/".$file[0]->filename);
        JurFiles::where('id', $file_id)->delete();
        $doc_data = Document::where('doc_number_old', $doc_number)->get();

        Journal::create(["author_id" => Auth::user()->id, "action" => Carbon::now('Asia/Qyzylorda')." - Удалена сканированная страница из регистрационных дел - <a target='_blank' href='/storage/documents/".$doc_number."/".$file[0]->filename."'>".$file[0]->filename."</a>", "document_id" => $doc_data[0]->id]);

        return back()->with('message', 'Файл успешно удален!');
    }

    public function edit(Document $document)
    {
        abort_if(Gate::denies('document_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $cities = City::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $districts = District::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $celevoe_naznachenies = Goal::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $fond_categories = FondCategory::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $object_types = ObjectType::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $object_groups = ObjectGroup::all()->pluck('name', 'id')->prepend(trans('global.pleaseSelect'), '');

        $document->load('city', 'district', 'celevoe_naznachenie', 'fond_category', 'object_type', 'object_group');

        $journal_actions = Journal::where('document_id', $document->id)->get();

        return view('admin.documents.edit', compact('cities', 'districts', 'celevoe_naznachenies', 'fond_categories', 'object_types', 'object_groups', 'document', 'journal_actions'));
    }

    public function update(UpdateDocumentRequest $request, Document $document)
    {
        $document->update($request->all());

        /*
        if (count($document->scaned_docs) > 0) {
            foreach ($document->scaned_docs as $media) {
                if (!in_array($media->file_name, $request->input('scaned_docs', []))) {
                    $media->delete();
                }
            }
        }

        $media = $document->scaned_docs->pluck('file_name')->toArray();

        foreach ($request->input('scaned_docs', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $document->addMedia(storage_path('tmp/uploads/' . $file))->toMediaCollection('scaned_docs');
            }
        }*/

        Journal::create(["author_id" => Auth::user()->id, "action" => Carbon::now('Asia/Qyzylorda')." - Данные документа изменены", "document_id" => $document->id]);

        return back()->with('message', 'Информация успешно обновлена!');
    }

    public function show(Document $document)
    {
        abort_if(Gate::denies('document_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $document->load('city', 'district', 'celevoe_naznachenie', 'fond_category', 'object_type', 'object_group');

        $files = Files::where('doc_id', $document->doc_number_old)->orderBy('num', 'ASC')->get();
        $journal_actions = Journal::where('document_id', $document->id)->get();

        return view('admin.documents.show', compact('document', 'journal_actions'));
    }

    public function destroy(Document $document)
    {
        abort_if(Gate::denies('document_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        Files::where('doc_id', $document->doc_number_old)->delete();
        Storage::deleteDirectory('/public/documents/'.$document->doc_number_old);
        $document->delete();
        Journal::where('document_id', $document->id)->delete();

        return back()->with('message', 'Документ успешно удален!');
    }

    public function massDestroy(MassDestroyDocumentRequest $request)
    {
        Document::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('document_create') && Gate::denies('document_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Document();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
