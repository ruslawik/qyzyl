<?php

namespace App\Http\Controllers\Admin;
use App\Models\City;
use App\Models\District;
use App\Models\Document;
use App\Models\FondCategory;
use App\Models\Goal;
use App\Models\ObjectGroup;
use App\Models\ObjectType;
use Illuminate\Http\Request;
use App\Models\Files;
use Illuminate\Support\Facades\DB;
use Auth;

class HomeController
{
    public function index()
    {
		$ar['name'] = Auth::user()->name;
        return view('home', $ar);
    }


    public function monitor(Request $request){

    		$query = DB::table('documents2');
    		$files = DB::table('files');
    		$jurFiles = DB::table('jur_files');
    		$jurFiles2 = DB::table('jur_files2');

    		$query->where('created_at', '>=', $request['date_from']." 00:00:00")->where('created_at', '<=', $request['date_to']." 00:00:00");
            $files->where('created_at', '>=', $request['date_from']." 00:00:00")->where('created_at', '<=', $request['date_to']." 00:00:00");
            $jurFiles->where('created_at', '>=', $request['date_from']." 00:00:00")->where('created_at', '<=', $request['date_to']." 00:00:00");
            $jurFiles2->where('created_at', '>=', $request['date_from']." 00:00:00")->where('created_at', '<=', $request['date_to']." 00:00:00");

            if($request->has('city_id')){
                if($request['city_id'] != -1){
                    $query->where('city_id', $request['city_id']);
                }
            }

            if($request->has('district_id')){
                if($request['district_id'] != -1){
                    $query->where('district_id', $request['district_id']);
                }
            }

            if($request->has('goal_id')){
                if($request['goal_id'] != -1){
                    $query->where('celevoe_naznachenie_id', $request['goal_id']);
                }
            }

            if($request->has('object_type_id')){
                if($request['object_type_id'] != -1){
                    $query->where('object_type_id', $request['object_type_id']);
                }
            }

            if($request->has('object_group_id')){
                if($request['object_group_id'] != -1){
                    $query->where('object_group_id', $request['object_group_id']);
                }
            }

            if($request->has('fond_cat_id')){
                if($request['fond_cat_id'] != -1){
                    $query->where('fond_category_id', $request['fond_cat_id']);
                }
            }
  			
  			$query->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as total'));
            $files->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as total_files'));
            $jurFiles->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as total_jur'));
            $jurFiles2->select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as total_jur2'));

            $query->groupBy(DB::raw('DATE(created_at)'));
            $files->groupBy(DB::raw('DATE(created_at)'));
            $jurFiles->groupBy(DB::raw('DATE(created_at)'));
            $jurFiles2->groupBy(DB::raw('DATE(created_at)'));

            $array['docs'] = $query->get()->toArray();
            $array['files'] = $files->get()->toArray();
            $array['jur'] = $jurFiles->get()->toArray();
            $array['jur2'] = $jurFiles2->get()->toArray();

            $arr1 = array_merge( $array['docs'], $array['files']);
            $result2 = array_merge($arr1, $array['jur']);
            $result = array_merge($result2, $array['jur2']);



            $dateArray = [];

            foreach($result as $key => $arr)
            {
                $dateArray[$key]=$arr->date;
            }

            array_multisort($dateArray, SORT_STRING, $result);


    	$cities          = City::get();
        $districts       = District::get();
        $goals           = Goal::get();
        $fond_categories = FondCategory::get();
        $object_types    = ObjectType::get();
        $object_groups   = ObjectGroup::get();

        return view('admin.monitor', compact('cities', 'districts', 'goals', 'fond_categories', 'object_types', 'object_groups', 'result'));

    }
}
?>
