<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyObjectGroupRequest;
use App\Http\Requests\StoreObjectGroupRequest;
use App\Http\Requests\UpdateObjectGroupRequest;
use App\Models\ObjectGroup;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ObjectGroupController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('object_group_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $objectGroups = ObjectGroup::all();

        return view('admin.objectGroups.index', compact('objectGroups'));
    }

    public function create()
    {
        abort_if(Gate::denies('object_group_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.objectGroups.create');
    }

    public function store(StoreObjectGroupRequest $request)
    {
        $objectGroup = ObjectGroup::create($request->all());

        return redirect()->route('admin.object-groups.index');
    }

    public function edit(ObjectGroup $objectGroup)
    {
        abort_if(Gate::denies('object_group_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.objectGroups.edit', compact('objectGroup'));
    }

    public function update(UpdateObjectGroupRequest $request, ObjectGroup $objectGroup)
    {
        $objectGroup->update($request->all());

        return redirect()->route('admin.object-groups.index');
    }

    public function show(ObjectGroup $objectGroup)
    {
        abort_if(Gate::denies('object_group_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.objectGroups.show', compact('objectGroup'));
    }

    public function destroy(ObjectGroup $objectGroup)
    {
        abort_if(Gate::denies('object_group_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $objectGroup->delete();

        return back();
    }

    public function massDestroy(MassDestroyObjectGroupRequest $request)
    {
        ObjectGroup::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
