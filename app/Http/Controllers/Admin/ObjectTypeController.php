<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyObjectTypeRequest;
use App\Http\Requests\StoreObjectTypeRequest;
use App\Http\Requests\UpdateObjectTypeRequest;
use App\Models\ObjectType;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ObjectTypeController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('object_type_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $objectTypes = ObjectType::all();

        return view('admin.objectTypes.index', compact('objectTypes'));
    }

    public function create()
    {
        abort_if(Gate::denies('object_type_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.objectTypes.create');
    }

    public function store(StoreObjectTypeRequest $request)
    {
        $objectType = ObjectType::create($request->all());

        return redirect()->route('admin.object-types.index');
    }

    public function edit(ObjectType $objectType)
    {
        abort_if(Gate::denies('object_type_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.objectTypes.edit', compact('objectType'));
    }

    public function update(UpdateObjectTypeRequest $request, ObjectType $objectType)
    {
        $objectType->update($request->all());

        return redirect()->route('admin.object-types.index');
    }

    public function show(ObjectType $objectType)
    {
        abort_if(Gate::denies('object_type_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.objectTypes.show', compact('objectType'));
    }

    public function destroy(ObjectType $objectType)
    {
        abort_if(Gate::denies('object_type_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $objectType->delete();

        return back();
    }

    public function massDestroy(MassDestroyObjectTypeRequest $request)
    {
        ObjectType::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
