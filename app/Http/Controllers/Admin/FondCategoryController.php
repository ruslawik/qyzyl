<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyFondCategoryRequest;
use App\Http\Requests\StoreFondCategoryRequest;
use App\Http\Requests\UpdateFondCategoryRequest;
use App\Models\FondCategory;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FondCategoryController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('fond_category_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $fondCategories = FondCategory::all();

        return view('admin.fondCategories.index', compact('fondCategories'));
    }

    public function create()
    {
        abort_if(Gate::denies('fond_category_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.fondCategories.create');
    }

    public function store(StoreFondCategoryRequest $request)
    {
        $fondCategory = FondCategory::create($request->all());

        return redirect()->route('admin.fond-categories.index');
    }

    public function edit(FondCategory $fondCategory)
    {
        abort_if(Gate::denies('fond_category_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.fondCategories.edit', compact('fondCategory'));
    }

    public function update(UpdateFondCategoryRequest $request, FondCategory $fondCategory)
    {
        $fondCategory->update($request->all());

        return redirect()->route('admin.fond-categories.index');
    }

    public function show(FondCategory $fondCategory)
    {
        abort_if(Gate::denies('fond_category_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.fondCategories.show', compact('fondCategory'));
    }

    public function destroy(FondCategory $fondCategory)
    {
        abort_if(Gate::denies('fond_category_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $fondCategory->delete();

        return back();
    }

    public function massDestroy(MassDestroyFondCategoryRequest $request)
    {
        FondCategory::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
