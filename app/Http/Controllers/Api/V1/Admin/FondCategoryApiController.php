<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreFondCategoryRequest;
use App\Http\Requests\UpdateFondCategoryRequest;
use App\Http\Resources\Admin\FondCategoryResource;
use App\Models\FondCategory;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class FondCategoryApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('fond_category_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new FondCategoryResource(FondCategory::all());
    }

    public function store(StoreFondCategoryRequest $request)
    {
        $fondCategory = FondCategory::create($request->all());

        return (new FondCategoryResource($fondCategory))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(FondCategory $fondCategory)
    {
        abort_if(Gate::denies('fond_category_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new FondCategoryResource($fondCategory);
    }

    public function update(UpdateFondCategoryRequest $request, FondCategory $fondCategory)
    {
        $fondCategory->update($request->all());

        return (new FondCategoryResource($fondCategory))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(FondCategory $fondCategory)
    {
        abort_if(Gate::denies('fond_category_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $fondCategory->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
