<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreObjectTypeRequest;
use App\Http\Requests\UpdateObjectTypeRequest;
use App\Http\Resources\Admin\ObjectTypeResource;
use App\Models\ObjectType;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ObjectTypeApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('object_type_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ObjectTypeResource(ObjectType::all());
    }

    public function store(StoreObjectTypeRequest $request)
    {
        $objectType = ObjectType::create($request->all());

        return (new ObjectTypeResource($objectType))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(ObjectType $objectType)
    {
        abort_if(Gate::denies('object_type_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ObjectTypeResource($objectType);
    }

    public function update(UpdateObjectTypeRequest $request, ObjectType $objectType)
    {
        $objectType->update($request->all());

        return (new ObjectTypeResource($objectType))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(ObjectType $objectType)
    {
        abort_if(Gate::denies('object_type_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $objectType->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
