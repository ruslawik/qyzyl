<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreObjectGroupRequest;
use App\Http\Requests\UpdateObjectGroupRequest;
use App\Http\Resources\Admin\ObjectGroupResource;
use App\Models\ObjectGroup;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ObjectGroupApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('object_group_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ObjectGroupResource(ObjectGroup::all());
    }

    public function store(StoreObjectGroupRequest $request)
    {
        $objectGroup = ObjectGroup::create($request->all());

        return (new ObjectGroupResource($objectGroup))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(ObjectGroup $objectGroup)
    {
        abort_if(Gate::denies('object_group_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ObjectGroupResource($objectGroup);
    }

    public function update(UpdateObjectGroupRequest $request, ObjectGroup $objectGroup)
    {
        $objectGroup->update($request->all());

        return (new ObjectGroupResource($objectGroup))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(ObjectGroup $objectGroup)
    {
        abort_if(Gate::denies('object_group_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $objectGroup->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
