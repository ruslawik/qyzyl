<?php

namespace App\Http\Requests;

use App\Models\Document;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreDocumentRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('document_create');
    }

    public function rules()
    {
        return [
            'barcode'     => [
                'string',
                'nullable',
            ],
            'cadastrnum'  => [
                'string',
                'nullable',
            ],
            'address'     => [
                'string',
                'nullable',
            ],
            'litera'      => [
                'string',
                'nullable',
            ],
            'object_name' => [
                'string',
                'nullable',
            ],
            'is_secret'   => [
                'required',
            ],
            'iin' => [
                'digits:12',
                'nullable',
            ]
        ];
    }
}
