<?php

namespace App\Http\Requests;

use App\Models\ObjectType;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreObjectTypeRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('object_type_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
        ];
    }
}
