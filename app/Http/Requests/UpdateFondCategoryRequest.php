<?php

namespace App\Http\Requests;

use App\Models\FondCategory;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateFondCategoryRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('fond_category_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
        ];
    }
}
