<?php

namespace App\Http\Requests;

use App\Models\ObjectGroup;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreObjectGroupRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('object_group_create');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
        ];
    }
}
