<?php

namespace App\Http\Requests;

use App\Models\ObjectGroup;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyObjectGroupRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('object_group_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:object_groups,id',
        ];
    }
}
