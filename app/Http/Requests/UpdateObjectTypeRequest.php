<?php

namespace App\Http\Requests;

use App\Models\ObjectType;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateObjectTypeRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('object_type_edit');
    }

    public function rules()
    {
        return [
            'name' => [
                'string',
                'required',
            ],
        ];
    }
}
