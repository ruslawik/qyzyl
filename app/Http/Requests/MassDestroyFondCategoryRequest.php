<?php

namespace App\Http\Requests;

use App\Models\FondCategory;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyFondCategoryRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('fond_category_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:fond_categories,id',
        ];
    }
}
