<?php

namespace App\Http\Requests;

use App\Models\ObjectType;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyObjectTypeRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('object_type_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:object_types,id',
        ];
    }
}
