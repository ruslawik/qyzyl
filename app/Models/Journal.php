<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Journal extends Model
{
    protected $table = "journal";

    protected $fillable = [
        'author_id',
        'action',
        'document_id'
    ];


    public function user ($author_id){

    	$user = User::where('id', $author_id)->get();
    	$user_name = $user[0]->name;
    	return $user_name;
    }
}
