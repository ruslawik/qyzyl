<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use \DateTimeInterface;
use App\Models\Files;
use App\Models\JurFiles;

class Document extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    public $table = 'documents2';

    protected $appends = [
        'scaned_docs',
    ];

    const IS_SECRET_SELECT = [
        'no'  => 'Нет',
        'yes' => 'Да',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
		'invent_num',
        'fio',
        'iin',
        'barcode',
        'city_id',
        'district_id',
        'cadastrnum',
        'address',
        'litera',
        'object_name',
        'celevoe_naznachenie_id',
        'fond_category_id',
        'object_type_id',
        'object_group_id',
        'barcodeint',
        'doc_number_old',
        'sheets_in_doc',
        'is_secret',
        'created_at',
        'updated_at',
        'deleted_at',
        'inv_nom_reg_del',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 120, 120);
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function celevoe_naznachenie()
    {
        return $this->belongsTo(Goal::class, 'celevoe_naznachenie_id');
    }

    public function fond_category()
    {
        return $this->belongsTo(FondCategory::class, 'fond_category_id');
    }

    public function object_type()
    {
        return $this->belongsTo(ObjectType::class, 'object_type_id');
    }

    public function object_group()
    {
        return $this->belongsTo(ObjectGroup::class, 'object_group_id');
    }
    
    public function getScanedDocsAttribute()
    {
        return $this->getMedia('scaned_docs');
    }

    public function files ($doc_id){

        return Files::where('doc_id', $doc_id)->orderBy('num', 'ASC')->get();
    }
	public function jur_files ($doc_id){

        return JurFiles::where('doc_id', $doc_id)->orderBy('num', 'ASC')->get();
    }

    public function jur_files2 ($doc_id){

        return JurFiles2::where('doc_id', $doc_id)->orderBy('num', 'ASC')->get();
    }
}
